import React from "react";

function Counter({ item, handleDelete, handleRefresh}) {
  const handleIncrement = () => {
    handleRefresh(item.id, item.count+1);
  };

  const handleDecrement = () => {
    if (item.count > 0) {
      handleRefresh(item.id, item.count-1);
    }
  };
  return (
    <div className="item-container" id={item.id}>
      <div className="zero">{item.count || "ZERO"}</div>
      <button onClick={handleIncrement} className="increment">
        +
      </button>
      <button onClick={handleDecrement} className="decrement">
        -
      </button>
      <button onClick={() => handleDelete(item.id)} className="delete">
        delete
      </button>
    </div>
  );
}

export default Counter;
