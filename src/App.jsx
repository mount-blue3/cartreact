import React, { useState } from "react";
import "./App.css";
import Counter from "./counter";

function App() {
  const defaultItems = [
    { id: 1, count: 0 },
    { id: 2, count: 0 },
    { id: 3, count: 0 },
    { id: 4, count: 0 },
  ];

  const [items, setItems] = useState(defaultItems);

  const handleDelete = (itemId) => {
    setItems((prevItems) => {
      return prevItems.filter((item) => item.id !== itemId);
    });
  };
  const handleRefresh = (itemId, newValue) => {
    setItems((prevItems) => {
      const newItemCount = prevItems.map((item) => {
        if (item.id === itemId) {
          return { ...item, count: newValue };
        }
        return item;
      });
      return newItemCount;
    });
  };

  const handleReset = () => {
    if (items.length === 0) setItems(defaultItems);
  };
  const handleRefreshAll = () => {
    const refreshedItems = items.map((item) => {
      return { ...item, count: 0 };
    });
    setItems(refreshedItems);
  };

  return (
    <div className="container">
      <div className="cart-container">
        <h2>Cart</h2>
        <h2 className="items">
          {items.filter((item) => item.count > 0).length}
        </h2>
        <h2>Items</h2>
      </div>
      <div className="reset-container">
        <button onClick={handleRefreshAll} className="refresh">
          Refresh
        </button>
        <button onClick={handleReset} className="reset">
          Reset
        </button>
      </div>
      {items.map((item) => (
        <Counter
          item={item}
          handleDelete={handleDelete}
          handleRefresh={handleRefresh}
        />
      ))}
    </div>
  );
}

export default App;
